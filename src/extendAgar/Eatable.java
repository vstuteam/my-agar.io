package extendAgar;

import com.golden.gamedev.object.Sprite;

/**
 * Created by kynew on 23.12.2015.
 */
public interface Eatable {
    Sprite getView();
    void update();
}
