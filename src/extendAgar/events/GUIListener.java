package extendAgar.events;

import extendAgar.Eatable;
import extendAgar.life.Bactery;
import extendAgar.specializations.Specialization;

import java.util.ArrayList;

/**
 * Created by Penskoy Nikita on 25.12.2015.
 */
public interface GUIListener extends Listener {
    void eatableAdded(Eatable s);
    void eatableDeleted(Eatable s);
    void readyToChangeSpecialization(ArrayList<Specialization> possible, Bactery s);
    void viewChanged(Bactery s);
}
