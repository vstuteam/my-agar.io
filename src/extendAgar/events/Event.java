package extendAgar.events;

import java.util.ArrayList;

/**
 * Created by Penskoy Nikita on 26.12.2015.
 */
public class Event {
    protected ArrayList<Listener> listeners;

    public Event() {
        listeners = new ArrayList<>();
    }

    public void addListener(Listener l) {
        listeners.add(l);
    }
}
