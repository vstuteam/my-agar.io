package extendAgar.events;

/**
 * Created by Penskoy Nikita on 26.12.2015.
 */
public interface GameListener extends Listener{
    void play();
    void pause();
}
