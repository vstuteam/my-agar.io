package extendAgar.factory;

import extendAgar.Eatable;

import java.util.Random;

/**
 * Created by kynew on 24.12.2015.
 */
public abstract class AbstractFactory {

    protected int min_size_element = 20;
    protected int max_size_element = 60;

    protected int heightField = 640;
    protected int widthField = 480;


    public AbstractFactory(int h, int w) {
        heightField = h;
        widthField = w;
    }

    public Eatable create() {
        return null;
    }

    protected Eatable create(int type) {
        return null;
    }

    protected int searchValue(int min, int max) {
        boolean find = false;
        Random rand = new Random();
        int result = 0;
        while (!find) {
            result = rand.nextInt(max + 1);
            if (min <= result && result <= max)
                find = true;
        }
        rand = null;
        return result;
    }
}
