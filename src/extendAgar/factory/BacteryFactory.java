package extendAgar.factory;

import extendAgar.life.AIBactery;
import extendAgar.life.Bactery;
import extendAgar.Eatable;
import extendAgar.util.Position;

import java.util.ArrayList;

/**
 * Created by kynew on 23.12.2015.
 */
public class BacteryFactory extends AbstractFactory {

    final static public int Player = 0;
    final static public int AI = 1;

    private ArrayList<Eatable> field;

    public BacteryFactory(int h, int w) {
        super(h, w);
        min_size_element = 50;
        max_size_element = 50;
    }

    public Eatable create(int isAI) {
        int x = 0, y = 0;
        boolean find = false;
        ArrayList<Boolean> facts = new ArrayList<>();
        while (!find) {
            x = searchValue(0, heightField);
            y = searchValue(0, widthField);
            double x2, y2, r2d2;
            facts.clear();
            if (field.size() == 0)
                find = true;
            for (int i = 0; i < field.size(); ++i) {
                x2 = ((Bactery) field.get(i)).getView().getX();
                y2 = ((Bactery) field.get(i)).getView().getY();
                r2d2 = ((Bactery) field.get(i)).getView().getWidth();
                facts.add(((x - x2) * (x - x2) + (y - y2) * (y - y2)) >= (r2d2 + max_size_element) * (r2d2 + max_size_element) + 5);
            }
            if (!facts.contains(false))
                find = true;
        }
        if (isAI == AI)
            return new AIBactery(new Position(x, y), max_size_element);
        return new Bactery(new Position(x, y), max_size_element);
    }

    public void setField(ArrayList<Eatable> field) {
        this.field = field;
    }
}
