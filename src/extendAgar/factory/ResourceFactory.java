package extendAgar.factory;

import extendAgar.Eatable;
import extendAgar.util.Position;
import javafx.util.Pair;
import extendAgar.resources.*;

import java.util.Random;

/**
 * Created by kynew on 23.12.2015.
 */
public class ResourceFactory extends AbstractFactory {

    final static public int AGARIO = 0;
    final static public int O2 = 1;
    final static public int CO2 = 2;
    final static public int WATER = 3;
    final static public int SUNLIGHT = 4;


    public ResourceFactory(int h, int w) {
        super(h, w);
        min_size_element = 20;
        max_size_element = 20;
    }

    public Eatable create() {
        Random rand = new Random();
        int type = rand.nextInt(5);
        rand = null;
        return create(type);
    }

    public Eatable create(int type) {
        int x, y;

        x = searchValue(min_size_element + 5, widthField - min_size_element - 5);
        y = searchValue(min_size_element + 5, heightField - min_size_element - 5);

        return create(type,x,y);
    }

    public Eatable create(int type, int x, int y) {

        int heigth = 0, width = 0;
        heigth = min_size_element;
        width = heigth;

        Resource eat = null;

        switch (type) {
            case AGARIO:
                eat = new Agar(new Position(x, y), width, heigth);
                break;
            case O2:
                eat = new O2(new Position(x, y), width, heigth);
                break;
            case CO2:
                eat = new CO2(new Position(x, y), width, heigth);
                break;
            case WATER:
                eat = new Water(new Position(x, y), width, heigth);
                break;
            case SUNLIGHT:
                eat = new Sunlight(new Position(x, y), width, heigth);
                break;
            default:
                eat = new Agar(new Position(x, y), width, heigth);
        }

        Random rand = new Random(System.currentTimeMillis());
        Pair<Double,Double> v = new Pair<>(rand.nextDouble() / 10.0 * (rand.nextInt(10)%2==0 ? 1 : -1)
                , rand.nextDouble() / 10.0 * (rand.nextInt(10)%2==0 ? 1 : -1));
        eat.getView().setHorizontalSpeed(v.getKey());
        eat.getView().setVerticalSpeed(v.getValue());

        return eat;
    }

}
