package extendAgar;

import com.golden.gamedev.object.Background;
import com.golden.gamedev.object.GameFont;
import com.golden.gamedev.object.background.ImageBackground;
import com.golden.gamedev.object.font.BitmapFont;
import com.golden.gamedev.object.font.SystemFont;
import extendAgar.events.GameListener;
import extendAgar.events.Listener;
import extendAgar.life.AIBactery;
import extendAgar.life.Bactery;
import extendAgar.resources.Resource;
import extendAgar.specializations.Specialization;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by Penskoy Nikita on 25.12.2015.
 */
public class GUI {

    protected ImageBackground back;
    protected int width = 1200,height = 980;

    protected GUIListener listener;
    protected GameEvent event;

    public GUI(GameListener l) {
        listener = new GUIListener();
        event = new GameEvent();
        event.addListener(l);

        setBackground();
    }

    public extendAgar.events.GUIListener getGUIListener() {
        return listener;
    }

    protected void setBackground() {
        BufferedImage img = null;
        try {
            img = ImageIO.read(getClass().getResource("res/back2.jpg"));
            width = img.getWidth();
            height = img.getHeight();
        } catch (java.io.IOException e) {
            img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2d = img.createGraphics();
            g2d.setBackground(Color.ORANGE);
        }

        back = new ImageBackground(img, width, height);
    }

    protected Background getBackground() {
        return back;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void render(Graphics2D graphics2D, PetriDish petri) {
        // clear background
        back.render(graphics2D);

        ArrayList<Eatable> all = petri.getAllEateble(Bactery.class);
        for(Eatable it : all)
        {
            ((Bactery)it).getView().render(graphics2D);
        }

        all = petri.getAllEateble(Resource.class);
        for(Eatable it : all)
        {
            ((Resource)it).getView().render(graphics2D);
        }

        GameFont font = new SystemFont(new Font(Font.SANS_SERIF,Font.BOLD,20),Color.CYAN);
        font.drawString(graphics2D,"Your size: " + petri.getPlayer().getSize(),
                5,5);
        font.drawString(graphics2D,
                "For update: " + petri.getPlayer().getSpecialization().toChange(petri.getPlayer().getSize()),
                5,30);
    }

    private class GUIListener implements extendAgar.events.GUIListener {

        @Override
        public void eatableAdded(Eatable s) {
            s.getView().setBackground(back);
        }

        @Override
        public void eatableDeleted(Eatable s) {
            if(s instanceof Bactery && !(s instanceof AIBactery)) {
                event.pauseGame();

                JOptionPane.showMessageDialog(null, "Игра закончена");

                System.exit(0);
            }
        }

        @Override
        public void readyToChangeSpecialization(ArrayList<Specialization> possible, Bactery s) {
            event.pauseGame();

            JComboBox cbox = new JComboBox();

            for (Specialization child : possible)
                cbox.addItem(child.toString());

            JButton j = new JButton("Выбрать");
            j.addActionListener(e -> {
                Window w = SwingUtilities.getWindowAncestor(j);

                if (w != null) {
                    w.setVisible(false);
                }
            });
            cbox.addKeyListener(new KeyListener() {
                @Override
                public void keyTyped(KeyEvent e) {
                    if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                        j.doClick();
                    }
                }

                @Override
                public void keyPressed(KeyEvent e) {

                }

                @Override
                public void keyReleased(KeyEvent e) {

                }
            });

            JOptionPane.showOptionDialog(null, "Ура!", "Вы выросли..", JOptionPane.DEFAULT_OPTION,
                    JOptionPane.PLAIN_MESSAGE, null, new Object[]{cbox,j}, null);

            s.changeSpecialization(possible.get(cbox.getSelectedIndex()));

            event.playGame();
        }

        @Override
        public void viewChanged(Bactery s) {
            s.getView().draw();
        }
    }

    private class GameEvent extends extendAgar.events.Event {

        public GameEvent() {
            super();
        }

        public void pauseGame() {
            for(Listener l : listeners)
                ((GameListener)l).pause();
        }

        public void playGame() {
            for(Listener l : listeners)
                ((GameListener)l).play();
        }
    }
}
