package extendAgar.specializations;

import extendAgar.factory.ResourceFactory;
import extendAgar.resources.O2;
import extendAgar.resources.Water;

import java.util.HashMap;

/**
 * Created by kynew on 23.12.2015.
 */
public class SimpleAnimal extends Specialization {

    static SimpleAnimal self = null;

    protected SimpleAnimal () {
        ration = new HashMap<>();

        ration.put(O2.class,1.0);
        ration.put(Water.class,3.0);
        ration.put(Primary.class,1.2);

        wasteProduct = ResourceFactory.CO2;

        upSize = 200;
    }

    static SimpleAnimal instance() {
        if(self == null)
            self = new SimpleAnimal();

        return self;
    }
}
