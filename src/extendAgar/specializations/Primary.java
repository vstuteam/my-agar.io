package extendAgar.specializations;

import extendAgar.factory.ResourceFactory;
import extendAgar.resources.Agar;
import extendAgar.resources.Sunlight;
import extendAgar.resources.Water;

import java.util.HashMap;

/**
 * Created by kynew on 23.12.2015.
 */
public class Primary extends Specialization {

    static Primary self = null;

    protected Primary () {
        ration = new HashMap<>();

        ration.put(Agar.class,7.0);
        ration.put(Sunlight.class,3.0);
        ration.put(Water.class,1.0);
        ration.put(Primary.class,1.0);

        wasteProduct = ResourceFactory.CO2;

        upSize = 100;
    }

    public static Primary instance() {
        if(self == null)
            self = new Primary();

        return self;
    }
}
