package extendAgar.specializations;

import java.util.*;

/**
 * Created by kynew on 23.12.2015.
 */
public class SpecializationTree {

    protected static SpecializationTree self = null;

    protected HashMap<Specialization,Specialization> parents;
    protected HashMap<Specialization,ArrayList<Specialization>> tree;
    protected ArrayList<Specialization> all;
    protected Random rand;

    protected SpecializationTree() {
        parents = new HashMap<>();
        tree = new HashMap<>();
        all = new ArrayList<>();
        rand = new Random(System.currentTimeMillis());
    }

    public static SpecializationTree instance() {
        if(self == null) {
            self = new SpecializationTree();
            self.addSpecialization(SimplePlant.instance(), Primary.instance());
            self.addSpecialization(SimpleAnimal.instance(), Primary.instance());
            self.addSpecialization(Herbivore.instance(), SimpleAnimal.instance());
            self.addSpecialization(Predator.instance(), SimpleAnimal.instance());
            self.addSpecialization(Omnivorous.instance(), SimpleAnimal.instance());
            self.addSpecialization(MossBactery.instance(), SimplePlant.instance());
            self.addSpecialization(ParasitePlant.instance(), SimplePlant.instance());
            self.addSpecialization(CarnivourPlant.instance(), SimplePlant.instance());
        }

        return self;
    }

    public void addSpecialization(Specialization specialization, Specialization parent) {
        parents.put(specialization,parent);

        if(!tree.containsKey(parent))
            tree.put(parent,new ArrayList<>());

        tree.get(parent).add(specialization);

        all.add(specialization);
    }

    public Specialization getRandomSpecialization() {
        return all.get(rand.nextInt(all.size()));
    }

    public ArrayList<Specialization> getAllChilds (Specialization parent) {
        ArrayList<Specialization> ans = new ArrayList<>();

        Queue<Specialization> q = new ArrayDeque<>();

        q.add(parent);

        while(!q.isEmpty()) {
            Specialization cur = q.poll();

            ans.add(cur);

            ArrayList<Specialization> childs = getChilds(cur);

            if(childs != null)
            {
                for(Specialization s : childs)
                    q.add(s);
            }
        }

        return ans;
    }

    public Specialization getParent(Specialization specialization) {
        return parents.get(specialization);
    }

    public ArrayList<Specialization> getChilds(Specialization specialization) {
        return tree.get(specialization);
    }

    public boolean canTransform(Specialization old, Specialization update) {
        return getParent(update) == old;
    }
}
