package extendAgar.specializations;

import extendAgar.factory.ResourceFactory;
import extendAgar.resources.CO2;
import extendAgar.resources.Sunlight;
import extendAgar.resources.Water;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Penskoy Nikita on 24.12.2015.
 */
public class ParasitePlant extends Specialization{

    static ParasitePlant self = null;

    protected ParasitePlant () {
        ration = new HashMap<>();

        ration.put(CO2.class,4.0);
        ration.put(Sunlight.class,4.0);
        ration.put(Water.class,4.0);

        ArrayList<Specialization> plants = SpecializationTree.instance().getAllChilds(SimplePlant.instance());
        for(Specialization plant : plants)
            ration.put(plant.getClass(), 1.5);

        wasteProduct = ResourceFactory.O2;

        upSize = 300;
    }

    static ParasitePlant instance() {
        if(self == null)
            self = new ParasitePlant();

        return self;
    }
}
