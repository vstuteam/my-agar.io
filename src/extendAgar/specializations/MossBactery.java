package extendAgar.specializations;

import extendAgar.factory.ResourceFactory;
import extendAgar.resources.CO2;
import extendAgar.resources.Sunlight;
import extendAgar.resources.Water;

import java.util.HashMap;

/**
 * Created by Penskoy Nikita on 24.12.2015.
 */
public class MossBactery extends Specialization{

    static MossBactery self = null;

    protected MossBactery () {
        ration = new HashMap<>();

        ration.put(CO2.class,7.0);
        ration.put(Sunlight.class,4.0);
        ration.put(Water.class,1.0);

        wasteProduct = ResourceFactory.O2;

        upSize = 300;
    }

    static MossBactery instance() {
        if(self == null)
            self = new MossBactery();

        return self;
    }
}
