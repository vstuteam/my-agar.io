package extendAgar.specializations;

import extendAgar.factory.ResourceFactory;
import extendAgar.resources.CO2;
import extendAgar.resources.Sunlight;
import extendAgar.resources.Water;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Penskoy Nikita on 24.12.2015.
 */
public class CarnivourPlant extends Specialization{

    static CarnivourPlant self = null;

    protected CarnivourPlant () {
        ration = new HashMap<>();

        ration.put(CO2.class,1.0);
        ration.put(Sunlight.class,7.0);
        ration.put(Water.class,3.0);

        ArrayList<Specialization> animals = SpecializationTree.instance().getAllChilds(SimpleAnimal.instance());
        for(Specialization animal : animals)
            ration.put(animal.getClass(), 0.5);

        wasteProduct = ResourceFactory.O2;

        upSize = 300;
    }

    static CarnivourPlant instance() {
        if(self == null)
            self = new CarnivourPlant();

        return self;
    }
}
