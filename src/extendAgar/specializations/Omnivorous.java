package extendAgar.specializations;

import extendAgar.factory.ResourceFactory;
import extendAgar.resources.O2;
import extendAgar.resources.Water;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Penskoy Nikita on 24.12.2015.
 */
public class Omnivorous extends Specialization{

    static Omnivorous self = null;

    protected Omnivorous () {
        ration = new HashMap<>();

        ration.put(O2.class,2.0);
        ration.put(Water.class,2.0);

        ArrayList<Specialization> plants = SpecializationTree.instance().getAllChilds(SimplePlant.instance());
        for(Specialization plant : plants)
            ration.put(plant.getClass(), 1.25);

        ArrayList<Specialization> animals = SpecializationTree.instance().getAllChilds(SimpleAnimal.instance());
        for(Specialization animal : animals)
            ration.put(animal.getClass(), 2.0);

        wasteProduct = ResourceFactory.CO2;

        upSize = 300;
    }

    static Omnivorous instance() {
        if(self == null)
            self = new Omnivorous();

        return self;
    }
}
