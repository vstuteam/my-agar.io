package extendAgar.specializations;

import extendAgar.factory.ResourceFactory;
import extendAgar.resources.CO2;
import extendAgar.resources.Sunlight;
import extendAgar.resources.Water;

import java.util.HashMap;

/**
 * Created by kynew on 23.12.2015.
 */
public class SimplePlant extends Specialization {

    static SimplePlant self = null;

    protected SimplePlant () {
        ration = new HashMap<>();

        ration.put(CO2.class,1.0);
        ration.put(Sunlight.class,3.0);
        ration.put(Water.class,7.0);
        ration.put(Primary.class,0.95);

        wasteProduct = ResourceFactory.CO2;

        upSize = 200;
    }

    static SimplePlant instance() {
        if(self == null)
            self = new SimplePlant();

        return self;
    }
}
