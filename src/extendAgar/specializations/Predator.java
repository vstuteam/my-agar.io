package extendAgar.specializations;

import extendAgar.factory.ResourceFactory;
import extendAgar.resources.O2;
import extendAgar.resources.Water;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Penskoy Nikita on 24.12.2015.
 */
public class Predator extends Specialization{

    static Predator self = null;

    protected Predator () {
        ration = new HashMap<>();

        ration.put(O2.class,1.0);
        ration.put(Water.class,1.0);

        ArrayList<Specialization> animals = SpecializationTree.instance().getAllChilds(SimpleAnimal.instance());
        for(Specialization animal : animals)
            ration.put(animal.getClass(), 2.0);

        wasteProduct = ResourceFactory.CO2;

        upSize = 300;
    }

    static Predator instance() {
        if(self == null)
            self = new Predator();

        return self;
    }
}
