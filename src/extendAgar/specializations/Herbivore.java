package extendAgar.specializations;

import extendAgar.factory.ResourceFactory;
import extendAgar.resources.O2;
import extendAgar.resources.Water;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Penskoy Nikita on 24.12.2015.
 */
public class Herbivore extends Specialization{

    static Herbivore self = null;

    protected Herbivore () {
        ration = new HashMap<>();

        ration.put(O2.class,4.0);
        ration.put(Water.class,6.0);

        ArrayList<Specialization> plants = SpecializationTree.instance().getAllChilds(SimplePlant.instance());
        for(Specialization plant : plants)
            ration.put(plant.getClass(), 1.25);

        wasteProduct = ResourceFactory.CO2;

        upSize = 300;
    }

    static Herbivore instance() {
        if(self == null)
            self = new Herbivore();

        return self;
    }
}
