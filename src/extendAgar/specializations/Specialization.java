package extendAgar.specializations;

import extendAgar.life.Bactery;
import extendAgar.Eatable;
import javafx.util.Pair;
import extendAgar.resources.Resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by kynew on 23.12.2015.
 */
public abstract class Specialization {

    Map<Class,Double> ration;
    int wasteProduct;
    int upSize;

    public boolean canEat(Eatable eat, int bacterySize) {
        return ration.containsKey(ration_class(eat)) &&
                (eat instanceof Resource || bacterySize*ration.get(ration_class(eat)) >= ((Bactery)eat).getSize());
    }

    protected Class ration_class(Eatable eat) {
        return eat instanceof Bactery ? ((Bactery) eat).getSpecialization().getClass() : eat.getClass();
    }

    public ArrayList<Eatable> throwProducts(ArrayList<Eatable> eats, int bacterySize) {
        ArrayList<Eatable> thrown = new ArrayList<>();

        for (Eatable eat : eats) {
            if (!canEat(eat,bacterySize))
                thrown.add(eat);
        }

        return thrown;
    }

    public Pair<ArrayList<Eatable>,Integer> tryGrowUp (ArrayList <Eatable> dish, int bacterySize) {
        Map<Class,Double> res = new HashMap<>();

        Set<Map.Entry<Class,Double>> set = ration.entrySet();

        for(Map.Entry<Class,Double> it : set)
        {
            res.put(it.getKey(),0.0);
        }

        int amount = bacterySize%upSize;
        for(Eatable it : dish)
        {
            Class eat = ration_class(it);
            if(res.containsKey(eat)) {
                if(Resource.class.isAssignableFrom(eat))
                    amount += ration.get(eat);
                else if(it instanceof Bactery)
                    amount += ration.get(eat)*((Bactery) it).getSize();
            }
        }

        ArrayList<Eatable> ans = new ArrayList<>();

        int growSize = amount/upSize;
        amount -= amount%upSize;

        for(Eatable it : dish)
        {
            Class eat = ration_class(it);
            double size = (it instanceof Bactery) ? ration.get(eat)*((Bactery) it).getSize() : ration.get(eat);

            if(res.containsKey(eat) && amount >= size)

            {
                ans.add(it);
                amount -= size;
            }
        }

        return new Pair<>(ans,growSize);
    }

    public int getWasteProduct() {
        return wasteProduct;
    }

    public String toString() {
        return this.getClass().getSimpleName();
    }

    public boolean readyForChange(int size) {
        return upSize <= size;
    }

    public int toChange(int size) {
        return Math.max(0,upSize - size);
    }
}
