package extendAgar.resources;

import extendAgar.Eatable;
import extendAgar.util.Position;
import extendAgar.view.ResourceView;

/**
 * Created by kynew on 23.12.2015.
 */
public abstract class Resource implements Eatable {

    protected ResourceView view;

    protected Position position;
    protected int width, height;

    protected final double SLOW_COFFICIENT = 0.0001;

    public Resource(Position pos, int w, int h) {
        position = pos;
        height = h;
        width = w;
        view = new ResourceView(this);

        view.draw();
    }

    public abstract int getType();

    public Position getPosition() {
        return position;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public ResourceView getView() {
        return view;
    }

    public void update() {
        double horizontalSpeed = getView().getHorizontalSpeed();
        horizontalSpeed += (SLOW_COFFICIENT) * (horizontalSpeed > 0 ? -1 : 1);
        getView().setHorizontalSpeed(horizontalSpeed);

        double verticalSpeed = getView().getVerticalSpeed();
        verticalSpeed += (SLOW_COFFICIENT) * (verticalSpeed > 0 ? -1 : 1);
        getView().setVerticalSpeed(verticalSpeed);
    }
}
