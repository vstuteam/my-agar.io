package extendAgar.resources;

import extendAgar.factory.ResourceFactory;
import extendAgar.util.Position;

/**
 * Created by kynew on 23.12.2015.
 */
public class Agar extends Resource {
    public Agar(Position pos, int w, int h) {
        super(pos, w, h);
    }

    @Override
    public int getType() {
        return ResourceFactory.AGARIO;
    }
}
