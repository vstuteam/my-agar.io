package extendAgar.resources;

import extendAgar.factory.ResourceFactory;
import extendAgar.util.Position;

/**
 * Created by kynew on 23.12.2015.
 */
public class CO2 extends Resource {
    public CO2(Position pos, int w, int h) {
        super(pos, w, h);
    }

    @Override
    public int getType() {
        return ResourceFactory.CO2;
    }
}
