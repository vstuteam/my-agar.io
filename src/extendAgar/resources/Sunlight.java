package extendAgar.resources;

import extendAgar.factory.ResourceFactory;
import extendAgar.util.Position;

/**
 * Created by kynew on 23.12.2015.
 */
public class Sunlight extends Resource {
    public Sunlight(Position pos, int w, int h) {
        super(pos, w, h);
    }

    @Override
    public int getType() {
        return ResourceFactory.SUNLIGHT;
    }
}
