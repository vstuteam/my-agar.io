package extendAgar.view;

import extendAgar.life.AIBactery;
import extendAgar.life.Bactery;
import extendAgar.life.Organism;
import extendAgar.specializations.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * Created by kynew on 24.12.2015.
 */
public class BacteryView extends AbstractView {

    public BacteryView(Organism par) {
        super(par, par.getPosition().getX(), par.getPosition().getY());

        if(!img.containsKey(Primary.class.getSimpleName())) {
            img.put(Primary.class.getSimpleName(), load_image("res/bacteria.png", Color.GREEN));
            img.put(SimpleAnimal.class.getSimpleName(), load_image("res/simple_animal.png", Color.GREEN));
            img.put(SimplePlant.class.getSimpleName(), load_image("res/simple_plant.png", Color.GREEN));
            img.put(Herbivore.class.getSimpleName(), load_image("res/herbivore.jpg", Color.GREEN));
            img.put(Predator.class.getSimpleName(), load_image("res/predator.png", Color.GREEN));
            img.put(Omnivorous.class.getSimpleName(), load_image("res/omnivorous.png", Color.GREEN));
            img.put(MossBactery.class.getSimpleName(), load_image("res/moss.jpg", Color.GREEN));
            img.put(CarnivourPlant.class.getSimpleName(), load_image("res/carnivorousplant.png", Color.GREEN));
            img.put(ParasitePlant.class.getSimpleName(), load_image("res/parasite.png", Color.GREEN));
            img.put(ParasitePlant.class.getSimpleName(), load_image("res/parasite.png", Color.GREEN));
        }

        this.setImage(new BufferedImage(par.getSize(), par.getSize(), BufferedImage.TYPE_INT_ARGB));
    }

    public void draw() {

        String name = ((Bactery) parent).getSpecialization().getClass().getSimpleName();
        Color main_color;
        BufferedImage img = this.img.get("");

        if (this.img.containsKey(name)) {
            img = this.img.get(name);
        }

        if (parent instanceof AIBactery) {
            main_color = Color.red;
        } else {
            main_color = Color.green;
        }

        draw(((Bactery) parent).getSize(),img, main_color);
    }

}
