package extendAgar.view;

import com.golden.gamedev.object.Sprite;
import extendAgar.Eatable;
import extendAgar.GameModel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;

/**
 * Created by kynew on 24.12.2015.
 */
public abstract class AbstractView extends Sprite {

    protected Eatable parent;

    public AbstractView(Eatable par, int x, int y) {
        parent = par;
        this.setX(x);
        this.setY(y);
    }

    public Eatable getParent() {
        return parent;
    }

    protected static HashMap<String,BufferedImage> img = null;
    {
        if(img == null) {
            img = new HashMap<>();
            img.put("", load_image("", Color.GRAY));
        }
    }

    protected static BufferedImage load_image(String path, Color back) {
        BufferedImage img = null;
        try {
            img = ImageIO.read(GameModel.class.getResource(path));
        } catch (java.io.IOException e) {
            img = new BufferedImage(10, 10, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2d = img.createGraphics();
            g2d.setBackground(back);
        }
        return img;
    }

    protected static HashMap<String,BufferedImage> cache = null;
    {
        if(cache == null)
            cache = new HashMap<>();
    }

    protected void draw(int radius, BufferedImage back, Color color) {

        BufferedImage image = null;

        String backName = back.toString() + String.valueOf(radius) + color.toString();

        if(AbstractView.cache.containsKey(backName))
            image = AbstractView.cache.get(backName);
        else {
            image = new BufferedImage(radius, radius, BufferedImage.TYPE_INT_ARGB);
            Graphics2D new_g2d = image.createGraphics();

            new_g2d.setColor(color);
            new_g2d.draw(new Ellipse2D.Double(0, 0, radius, radius));

            new_g2d.setClip(new Ellipse2D.Double(0, 0, radius, radius));
            new_g2d.drawImage(back, 0, 0, radius, radius, null);

            AbstractView.cache.put(backName,image);
        }

        this.setImage(image);
    }

    protected void draw(int radius, Color main_color, Color invisible, Color text_color, String name) {
        BufferedImage image = new BufferedImage(radius, radius, BufferedImage.TYPE_INT_ARGB);
        Graphics2D new_g2d = image.createGraphics();

        new_g2d.setColor(main_color);
        new_g2d.setBackground(invisible);

        new_g2d.fillOval(0, 0, radius, radius);

        new_g2d.setPaint(text_color);
        new_g2d.setFont(new Font("Serif", Font.BOLD, 20));

        FontMetrics fm = new_g2d.getFontMetrics();

        int x = image.getWidth() / 2 - fm.stringWidth(name) / 2;
        int y = image.getHeight() / 2 + fm.getHeight() / 3;

        new_g2d.drawString(name, x, y);

        this.setImage(image);
    }

}
