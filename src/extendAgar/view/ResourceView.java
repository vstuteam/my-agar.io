package extendAgar.view;

import extendAgar.resources.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;

/**
 * Created by kynew on 24.12.2015.
 */
public class ResourceView extends AbstractView {

    public ResourceView(Resource par) {
        super(par, par.getPosition().getX(), par.getPosition().getY());
        this.setImage(new BufferedImage(par.getHeight(), par.getHeight(), BufferedImage.TYPE_INT_ARGB));

        if(!img.containsKey(Agar.class.getSimpleName())) {
            img.put(Agar.class.getSimpleName(), load_image("res/agar.jpg", Color.RED));
            img.put(CO2.class.getSimpleName(), load_image("res/CO2.png", Color.CYAN));
            img.put(O2.class.getSimpleName(), load_image("res/O2.png", Color.WHITE));
            img.put(Sunlight.class.getSimpleName(), load_image("res/sunlight.png", Color.YELLOW));
            img.put(Water.class.getSimpleName(), load_image("res/water.png", Color.BLUE));
        }
    }

    public void draw() {

        BufferedImage img = this.img.get("");

        if (this.img.containsKey(parent.getClass().getSimpleName())) {
            img = this.img.get(parent.getClass().getSimpleName());
        }

        draw(((Resource) parent).getHeight(),img, new Color(0,0,0,0));
    }
}
