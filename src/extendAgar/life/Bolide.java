package extendAgar.life;

import com.golden.gamedev.object.Sprite;
import extendAgar.Eatable;
import extendAgar.util.Position;

import java.util.ArrayList;

/**
 * Created by kynew on 23.12.2015.
 */
public class Bolide extends Organism {

    private Sprite view;

    private Bactery parent;

    Bolide() {
        super();
    }

    Bolide(Position pos, int s) {
        super(pos, s);
    }

    public void die() {
    }

    public void update() {

    }

    private void growUp() {

    }

    public Bactery getParent() {
        return parent;
    }

    public Sprite getView() {
        return view;
    }
}
