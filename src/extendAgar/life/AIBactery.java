package extendAgar.life;

import extendAgar.Eatable;
import extendAgar.PetriDish;
import extendAgar.util.Position;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

import org.jpl7.*;
import sun.misc.IOUtils;

/**
 * Created by kynew on 23.12.2015.
 */
public class AIBactery extends Bactery {

    public AIBactery(Position pos, int s) {
        super(pos, s);
    }

    protected int MAX_DIST = 100;
    protected int TICKS = 50;
    protected static Random rand = null;
    {
        if(rand == null)
            rand = new Random(System.currentTimeMillis());
    }

    protected static File f = null;
    {
        if(f == null)
        {
            InputStream in = getClass().getResourceAsStream("aibactery.pl");
            try {
                f = File.createTempFile("aibactery", ".pl");

                byte[] buffer = new byte[in.available()];
                in.read(buffer);

                OutputStream outStream = new FileOutputStream(f);
                outStream.write(buffer);

                Query.oneSolution("consult('" + f.getAbsolutePath().replace("\\","\\\\") + "')");

                f.deleteOnExit();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void update() {

        if(TICKS > 0)
        {
            TICKS--;
            return;
        }else{
            TICKS = rand.nextInt(50);
        }

        ArrayList<Eatable> eatables = petriDish.getAllEatable();
        String s = "life([";
        MAX_DIST = 0;

        for(Eatable eatable : eatables) {
            if(eatable == this)
                continue;

            int x = (int) eatable.getView().getX();
            int y = (int) eatable.getView().getY();
            int dist = (int) getView().getDistance(eatable.getView());
            String enemy = eatable instanceof Bactery ?
                    (((Bactery) eatable).canEat(this) ? "true" : "false") : "false";
            String dish = canEat(eatable) ? "true" : "false";

            MAX_DIST = Math.max(MAX_DIST,dist);

            if (s.length() > "life([".length())
                s += ",";

            s += "[" + x + "," + y + "," + dist + "," + enemy + "," + dish + "]";
        }

        s += "],";

        int myX = (int) getView().getX();
        int myY = (int) getView().getY();

        s += "[" + myX + "," + myY + "], " + MAX_DIST + ",X)";

        Term t[] = Query.oneSolution(s).get("X").toTermArray();

        int x = t[0].intValue(), y = t[1].intValue();

        if (x < myX)
            view.setHorizontalSpeed(getHorizontalSpeed(true));
        else if (x == myX)
            view.setHorizontalSpeed(0);
        else if (x > myX)
            view.setHorizontalSpeed(getHorizontalSpeed(false));
        
        if (y < myY)
            view.setVerticalSpeed(getVerticalSpeed(true));
        else if (y == myY)
            view.setVerticalSpeed(0);
        else if (y > myY)
            view.setVerticalSpeed(getVerticalSpeed(false));
    }
}
