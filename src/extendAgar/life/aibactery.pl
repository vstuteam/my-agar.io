
life(Eatable, Me, Max_dist, X) :-
    closest_enemy(Eatable, Me, Max_dist, X);
    closest_eat(Eatable, Me, X);
    stay(Me, X).

closest_enemy(Eatable, Me, Max_dist, X) :-
    get_enemy(Eatable,Enemy),
    convert_to_dist(Enemy,Dist,Me),
    min_list(Dist,D,I),
    (D =< Max_dist ->
        (nth0(I,Enemy,[EX,EY,_,_,_]),getReverse(Me, [EX,EY], X)) , true).

closest_eat(Eatable, Me, X) :-
    get_eat(Eatable,Eat),
    convert_to_dist(Eat,Dist,Me),
    min_list(Dist,_,I),
    nth0(I,Eat,[EX,EY,_,_,_]),
    X = [EX,EY], !.

convert_to_dist([[_,_,DD,_,_]|T],[D|Dist], Me) :-
    D = DD,
    convert_to_dist(T,Dist, Me).
convert_to_dist([],[],_).

min_list(L, Min, I) :-
   member(Min, L), \+((member(N, L), N < Min)),
   nth0(I, L, Min).

get_enemy([[X,Y,D,En,We]|Eatable], Enemy2) :-
   ( En -> Enemy2 = [[X2,Y2,D2,En2,We2]|Enemy], [X2,Y2,D2,En2,We2] = [X,Y,D,En,We],get_enemy(Eatable,Enemy) ;
        get_enemy(Eatable,Enemy2) ).
get_enemy([],[]).

get_eat([[X,Y,D,En,We]|Eatable], Eat2) :-
   ( We -> Eat2 = [[X2,Y2,D2,En2,We2]|Eat], [X2,Y2,D2,En2,We2] = [X,Y,D,En,We],get_eat(Eatable,Eat) ;
        get_eat(Eatable,Eat2) ).
get_eat([],[]).

stay([X,Y], Ans) :- Ans = [X,Y].

getReverse([MeX,MeY], [RX,RY],[X,Y]) :-
    X is 2*MeX - RX, Y is 2*MeY - RY.