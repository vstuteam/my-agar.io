package extendAgar.life;

import extendAgar.Eatable;
import extendAgar.PetriDish;
import extendAgar.util.Position;

import java.util.ArrayList;

/**
 * Created by kynew on 23.12.2015.
 */
public abstract class Organism implements Eatable {

    protected int size;
    protected Position position;
    protected ArrayList<Eatable> eaten;
    protected PetriDish petriDish;

    public Organism() {
    }

    public Organism(Position pos, int s) {
        eaten = new ArrayList<>();
        position = pos;
        size = s;
    }

    public void setPetriDish(PetriDish petriDish) {
        this.petriDish = petriDish;
    }

    public boolean canEat(Eatable dish) {
        return false;
    }

    public boolean eat(Eatable dish) {
        return false;
    }

    protected ArrayList<Eatable> throwWastedProducts() {
        return new ArrayList<>();
    }

    protected void updateSize() {

    }

    public void die() {
    }

    public void update() {

    }

    public Position getPosition() {
        return position;
    }

    public int getSize() {
        return size;
    }
}
