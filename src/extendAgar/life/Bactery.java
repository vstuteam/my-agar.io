package extendAgar.life;

import extendAgar.Eatable;
import extendAgar.GameModel;
import extendAgar.util.Position;
import extendAgar.view.BacteryView;
import javafx.util.Pair;
import extendAgar.resources.Resource;
import extendAgar.specializations.Primary;
import extendAgar.specializations.Specialization;
import extendAgar.specializations.SpecializationTree;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by kynew on 23.12.2015.
 */
public class Bactery extends Organism {

    protected final int WASTE_FREQUENCY = 5;
    protected final double MAX_SPEED = 0.13;
    protected final int SIZE_SCALE = 10;

    protected BacteryView view;
    protected Specialization specialization;

    public Bactery(Position pos, int s) {
        this(pos,s,Primary.instance());
    }

    public Bactery(Position pos, int s, Specialization spec) {
        super(pos, s);

        specialization = spec;
        view = new BacteryView(this);

        view.draw();
    }

    public double getVerticalSpeed(boolean up) {
        if(up)
            return Math.min(-MAX_SPEED + getSize()/1000,-0.01);
        else
            return Math.max(MAX_SPEED - getSize()/1000, 0.01);
    }

    public double getHorizontalSpeed(boolean left) {
        if(left)
            return Math.min(-MAX_SPEED + getSize()/1000,-0.01);
        else
            return Math.max(MAX_SPEED - getSize()/1000, 0.01);
    }

    public void changeSpecialization(Specialization specialization) {
        if(SpecializationTree.instance().canTransform(this.specialization,specialization)) {
            this.specialization = specialization;

            ArrayList<Eatable> eat = specialization.throwProducts(eaten,getSize());

            for(Eatable e : eat) {
                if (e instanceof Resource) {
                    Resource r = (Resource) petriDish.getResourceFactory()
                            .create(((Resource) e).getType(),
                                    (int) getView().getX(),
                                    (int) getView().getY());
                    petriDish.addEateble(r);
                }
            }

            eaten = difference(eaten,eat);
        }
    }

    public Specialization getSpecialization() {
        return specialization;
    }

    public BacteryView getView() {
        return view;
    }

    public boolean canEat(Eatable dish) {
        return specialization.canEat(dish,getSize());
    }

    public boolean eat(Eatable dish) {
        int added = 0;


        if(dish instanceof Resource) {
            petriDish.deleteEateble(dish);
            eaten.add(dish);
            added++;
        } else if(dish instanceof Bactery) {
            ((Bactery) dish).die();
            eaten.add(dish);
            added += ((Bactery) dish).getSize()/SIZE_SCALE;
        }

        throwWastedProducts(added);

        updateSize();

        return true;
    }

    protected ArrayList<Eatable> throwWastedProducts(int dish_size) {
        ArrayList<Eatable> thrown = new ArrayList<>();
        for(int i=0; i< dish_size/5; ++i) {

            Resource r = (Resource) petriDish.getResourceFactory()
                    .create(specialization.getWasteProduct(),
                            (int) getView().getX(),
                            (int) getView().getY());
            petriDish.addEateble(r);
            thrown.add(r);
        }

        return thrown;
    }

    protected void updateSize() {
        Pair<ArrayList<Eatable>,Integer> grow = specialization.tryGrowUp(eaten,getSize());

        eaten = difference(eaten,grow.getKey());
        size += grow.getValue() * SIZE_SCALE;

        if(specialization.readyForChange(getSize())) {
            readyToChangeSpecialization();
        }

        petriDish.viewChanged(this);
    }

    protected static <T> ArrayList<T> difference(ArrayList<T> one, ArrayList<T> two) {
        ArrayList<T> diff = new ArrayList<>();

        for(T it : one) {
            if(!two.contains(it))
                diff.add(it);
        }

        return diff;
    }

    protected void readyToChangeSpecialization() {
        ArrayList<Specialization> childs = SpecializationTree.instance().getChilds(specialization);

        if(childs != null) {
            if (this instanceof AIBactery) {
                Random rand = new Random();
                changeSpecialization(childs.get(rand.nextInt(childs.size())));
            } else {
                petriDish.readyToChangeSpecialization(childs, this);
            }
        }
    }

    public void die() {
        getView().setActive(false);
        petriDish.deleteEateble(this);

        for (Eatable anEaten : eaten) {
            if(anEaten instanceof Resource) {
                Resource r = (Resource) petriDish.getResourceFactory()
                        .create(((Resource) anEaten).getType(),
                                (int) getView().getX(),
                                (int) getView().getY());
                petriDish.addEateble(r);
            }
        }
    }

    public void update() {

    }

    public Position getPosition() {
        return position;
    }

    public int getSize() {
        return size;
    }
}
