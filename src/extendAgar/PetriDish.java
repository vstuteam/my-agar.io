package extendAgar;

import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.SpriteGroup;
import com.golden.gamedev.object.collision.CollisionBounds;
import extendAgar.events.Event;
import extendAgar.events.GUIListener;
import extendAgar.events.Listener;
import extendAgar.factory.BacteryFactory;
import extendAgar.factory.ResourceFactory;
import extendAgar.life.Bactery;
import extendAgar.specializations.Specialization;
import extendAgar.util.CollisionManager;
import extendAgar.resources.Resource;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;

/**
 * Created by kynew on 23.12.2015.
 */
public class PetriDish {

    protected int width, height;

    protected ResourceFactory resourceFactory;
    protected BacteryFactory bacteryFactory;

    private ArrayList<Eatable> life;
    private Bactery player;

    protected GUIEvent event;
    private Queue<Eatable> add_queue;
    private Queue<Eatable> delete_queue;

    public PetriDish(int width, int height) {
        event = new GUIEvent();

        add_queue = new ArrayDeque<>();
        delete_queue = new ArrayDeque<>();

        this.life = new ArrayList<>();

        this.width = width;
        this.height = height;

        this.resourceFactory = new ResourceFactory(height,width);
        this.bacteryFactory = new BacteryFactory(height,width);
    }

    public ResourceFactory getResourceFactory() {
        return resourceFactory;
    }

    public BacteryFactory getBacteryFactory() {
        return bacteryFactory;
    }

    public void addEateble(Eatable eatable){

        life.add(eatable);

        if(eatable instanceof Bactery)
            ((Bactery) eatable).setPetriDish(this);

        add_queue.add(eatable);
    }

    public void update_add_queue() {
        while(!add_queue.isEmpty())
        {
            event.addEatable(add_queue.poll());
        }

        while(!delete_queue.isEmpty()) {
            event.deleteEatable(delete_queue.poll());
        }
    }

    public void addPlayer(Bactery player) {
        this.player = player;
        addEateble(player);
    }

    public Bactery getPlayer() {
        return player;
    }

    public void deleteEateble(Eatable eatable){
        eatable.getView().setActive(false);

        life.remove(eatable);
        delete_queue.add(eatable);

        if(player == eatable)
            player = null;
    }


    public ArrayList<Eatable> getAllEateble(Class obj) {
        ArrayList<Eatable> result = new ArrayList<>();
        for(int i=0; i< life.size(); ++i){
            Eatable eat = life.get(i);
            if(obj.isInstance(eat))
                result.add(eat);
        }
        return result;
    }

    public ArrayList<Eatable> getAllEatable() {
        ArrayList<Eatable> result = new ArrayList<>();
        for(int i=0; i< life.size(); ++i){
            Eatable eat = life.get(i);
            result.add(eat);
        }
        return result;
    }

    public void addListener(GUIListener l) {
        event.addListener(l);
    }

    public void readyToChangeSpecialization(ArrayList<Specialization> possible, Bactery s) {
        event.chooseSpecialization(possible, s);
    }

    public void viewChanged(Bactery s) {
        event.viewChanged(s);
    }

    private class GUIEvent extends Event{

        public GUIEvent() {
            super();
        }

        public void addEatable(Eatable s) {

            for(Listener l : listeners)
                ((GUIListener)l).eatableAdded(s);
        }

        public void deleteEatable(Eatable s) {

            for(Listener l : listeners)
                ((GUIListener)l).eatableDeleted(s);
        }

        public void chooseSpecialization(ArrayList<Specialization> possible, Bactery s) {
            for(Listener l : listeners)
                ((GUIListener)l).readyToChangeSpecialization(possible, s);
        }

        public void viewChanged(Bactery s) {
            for(Listener l : listeners)
                ((GUIListener)l).viewChanged(s);
        }
    }
}
