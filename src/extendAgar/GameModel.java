package extendAgar;

import com.golden.gamedev.Game;
import com.golden.gamedev.GameLoader;
import com.golden.gamedev.object.CollisionManager;
import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.SpriteGroup;
import extendAgar.factory.BacteryFactory;
import extendAgar.life.Bactery;
import extendAgar.resources.Resource;
import extendAgar.specializations.Specialization;
import extendAgar.util.CollisionBoundsManager;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Random;

/**
 * Main class of ExtendAgar.
 * Responsible for update model, fill collision managers.
 */
public class GameModel extends Game {

    /**
     * Width of visible part of field.
     */
    protected static int width = 640;
    /**
     * Height of visible part of field.
     */
    protected static int height = 480;

    /**
     * List of collision managers.
     */
    protected ArrayList<CollisionManager> collisionManager;

    /**
     * Petri dish (game field).
     */
    protected PetriDish petriDish;

    /**
     * GUI.
     */
    protected GUI gui;

    /**
     * Sprite group of resources sprites.
     * Used by collision managers.
     */
    protected SpriteGroup resources;

    /**
     * Sprite group of bacterias sprites.
     * Used by collision managers.
     */
    protected SpriteGroup bacterias;

    /**
     * How often can create new objects on petri dish.
     * Use as maximum bound of random.
     */
    protected int CREATE_RARITY = 40;

    /**
     * Max resource create count by one time.
     */
    protected int MAX_RESOURCE_CREATE_COUNT = 4;

    /**
     * Max bacteria create count by one time.
     */
    protected int MAX_BACTERIA_CREATING = 2;

    /**
     * Start resource count on petri dish.
     */
    protected int START_RESOURCE_COUNT = 500;

    /**
     * Start bacteria count on petri dish.
     */
    protected int START_BACTERIA_COUNT = 10;

    /**
     * Default constructor.
     */
    protected GameModel() {
    }

    public PetriDish getPetriDish() {
        return petriDish;
    }

    @Override
    public void initResources() {
        gui = new GUI(new GameListener());

        this.petriDish = new PetriDish(gui.getWidth(), gui.getHeight());
        this.collisionManager = new ArrayList<>();

        petriDish.addListener(new GUIListener());
        petriDish.addListener(gui.getGUIListener());

        fillCollisionManager();

        createResources(START_RESOURCE_COUNT);
        createPlayer();
        createBacteries(START_BACTERIA_COUNT);
    }

    /**
     * Fill collision manager with collision groups.
     */
    protected void fillCollisionManager() {
        resources = new SpriteGroup("resources");
        bacterias = new SpriteGroup("bacterias");

        CollisionManager cm = new extendAgar.util.CollisionManager();
        cm.setCollisionGroup(resources, bacterias);
        collisionManager.add(cm);

        cm = new extendAgar.util.CollisionManager();
        cm.setCollisionGroup(bacterias, bacterias);
        collisionManager.add(cm);

        cm = new CollisionBoundsManager(gui.getBackground());
        cm.setCollisionGroup(resources, null);
        collisionManager.add(cm);

        cm = new CollisionBoundsManager(gui.getBackground());
        cm.setCollisionGroup(bacterias, null);
        collisionManager.add(cm);
    }

    public void createPlayer() {
        petriDish.getBacteryFactory().setField(petriDish.getAllEateble(Bactery.class));
        petriDish.addPlayer((Bactery) petriDish.getBacteryFactory().create(BacteryFactory.Player));
    }

    public void createResources(int count) {
        for (int i = 0; i < count; ++i) {
            Eatable eatable = petriDish.getResourceFactory().create();
            petriDish.addEateble(eatable);
        }
    }

    public void createBacteries(int count) {
        for (int i = 0; i < count; ++i) {
            petriDish.getBacteryFactory().setField(petriDish.getAllEateble(Bactery.class));

            Eatable eatable = petriDish.getBacteryFactory().create(BacteryFactory.AI);
            petriDish.addEateble(eatable);
        }
    }

    /**
     * Create some (maybe 0) new objects on petri dish.
     * @param l ticks from previous call
     */
    public void deltaCreate(long l) {
        Random rand = new Random(System.currentTimeMillis());

        int res = rand.nextInt(CREATE_RARITY);

        if (res == 0) {
            int count = rand.nextInt(MAX_RESOURCE_CREATE_COUNT);
            createResources(count);

            count = rand.nextInt(MAX_BACTERIA_CREATING);
            createBacteries(count);
        }
    }

    @Override
    public void update(long l) {

        deltaCreate(l);

        checkKeysAndMove();

        ArrayList<Eatable> all = petriDish.getAllEatable();
        for (Eatable it : all) {
            it.update();
            it.getView().update(l);
        }

        Sprite pl = petriDish.getPlayer().getView();
        pl.getBackground().setToCenter(pl);

        for (CollisionManager aCollisionManager : collisionManager)
            aCollisionManager.checkCollision();
        petriDish.update_add_queue();
    }

    @Override
    public void render(Graphics2D graphics2D) {
        gui.render(graphics2D, petriDish);
    }

    /**
     * Move based on down keys.
     */
    public void checkKeysAndMove() {
        double speedX = 0, speedY = 0;
        if (keyDown(KeyEvent.VK_LEFT)) speedX = petriDish.getPlayer().getHorizontalSpeed(true);
        if (keyDown(KeyEvent.VK_RIGHT)) speedX = petriDish.getPlayer().getHorizontalSpeed(false);
        petriDish.getPlayer().getView().setHorizontalSpeed(speedX);

        if (keyDown(KeyEvent.VK_DOWN)) speedY = petriDish.getPlayer().getVerticalSpeed(false);
        if (keyDown(KeyEvent.VK_UP)) speedY = petriDish.getPlayer().getVerticalSpeed(true);
        petriDish.getPlayer().getView().setVerticalSpeed(speedY);
    }

    public GUI getGui() {
        return gui;
    }

    public static void main(String[] args) {
        GameLoader game = new GameLoader();
        GameModel model = new GameModel();
        game.setup(model, new Dimension(width, height), false);
        game.start();
    }

    /**
     * Class responsible for update sprite groups.
     */
    private class GUIListener implements extendAgar.events.GUIListener {

        @Override
        public void eatableAdded(Eatable s) {
            if (s instanceof Bactery)
                bacterias.add(s.getView());
            else if (s instanceof Resource)
                resources.add(s.getView());
        }

        @Override
        public void eatableDeleted(Eatable s) {
            if (s instanceof Bactery)
                bacterias.remove(s.getView());
            else if (s instanceof Resource)
                resources.remove(s.getView());
        }

        @Override
        public void readyToChangeSpecialization(ArrayList<Specialization> possible, Bactery s) {
        }

        @Override
        public void viewChanged(Bactery s) {
        }
    }

    /**
     * Class responsible for game pause.
     */
    private class GameListener implements extendAgar.events.GameListener {
        @Override
        public void play() {
            start();
        }

        @Override
        public void pause() {
            stop();
        }
    }
}
