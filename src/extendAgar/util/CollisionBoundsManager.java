package extendAgar.util;

import com.golden.gamedev.object.Background;
import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.collision.CollisionBounds;

/**
 * Created by Penskoy Nikita on 25.12.2015.
 */
public class CollisionBoundsManager extends CollisionBounds {

    public CollisionBoundsManager(Background back) {
        super(back);
    }

    @Override
    public void collided(Sprite sprite) {
        revertPosition1();
    }
}
