package extendAgar.util;

import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.collision.BasicCollisionGroup;
import extendAgar.life.Bactery;
import extendAgar.view.BacteryView;
import extendAgar.view.ResourceView;

/**
 * Created by kynew on 23.12.2015.
 */
public class CollisionManager extends BasicCollisionGroup {

    public CollisionManager() {
        pixelPerfectCollision = true;
    }

    public void collided(Sprite s1, Sprite s2) {

        if ((s1 instanceof BacteryView && s2 instanceof ResourceView) || (s2 instanceof BacteryView && s1 instanceof ResourceView)) {
            if (s1 instanceof BacteryView) {
                if (((Bactery) ((BacteryView) s1).getParent()).canEat(((ResourceView) s2).getParent()))
                    ((Bactery) ((BacteryView) s1).getParent()).eat(((ResourceView) s2).getParent());
            } else {
                if (((Bactery) ((BacteryView) s2).getParent()).canEat(((ResourceView) s1).getParent()))
                    ((Bactery) ((BacteryView) s2).getParent()).eat(((ResourceView) s1).getParent());
            }
        } else if (s1 instanceof BacteryView && s2 instanceof BacteryView) {
            if (((Bactery) ((BacteryView) s1).getParent()).canEat(((BacteryView) s2).getParent())) {
                ((Bactery) ((BacteryView) s1).getParent()).eat(((BacteryView) s2).getParent());
            } else if (((Bactery) ((BacteryView) s2).getParent()).canEat(((BacteryView) s1).getParent())) {
                ((Bactery) ((BacteryView) s2).getParent()).eat(((BacteryView) s1).getParent());
            }
        }
    }
}
